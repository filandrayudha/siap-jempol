<!-- SIDEBAR -->
<aside id="main-sidebar" class="nicescroll">
    <div class="menu-section">
        <ul class="nav side-menu">
            <li class="active"><a href="<?php echo site_url('admin/dashboard'); ?>"><i class="icon ion-home"></i> <span class="title"><?php echo lang('text_dashboard') ?></span></a></li>

            <li><a><i class="icon ion-images"></i> <span class="title"><?php echo lang('text_media') ?></span> <i class="icon ion-ios-arrow-left menu-carret"></i></a>
                <ul class="nav child-menu">
                    <li><a href="<?php echo site_url('admin/media') ?>"><?php echo lang('text_libraries') ?></a></li>
                    <li><a href="<?php echo site_url('admin/media/add') ?>"><?php echo lang('text_add_media') ?></a></li>
                </ul>
            </li>
            <li><a><i class="icon ion-tshirt"></i> <span class="title"><?php echo lang('text_plugins') ?></span> <i class="icon ion-ios-arrow-left menu-carret"></i></a>
                <ul class="nav child-menu">
                    <li><a href="<?php echo site_url('admin/plugin') ?>"><?php echo lang('text_all_plugins') ?></a></li>
                    <li><a href="<?php echo site_url('admin/plugin/add') ?>"><?php echo lang('text_add_plugin') ?></a></li>
                </ul>
            </li>

            <li><a><i class="icon ion-wrench"></i> <span class="title"><?php echo lang('text_tools') ?></span> <i class="icon ion-ios-arrow-left menu-carret"></i></a>
                <ul class="nav child-menu">
                    <li><a href="<?php echo site_url('admin/tool/backup') ?>"><?php echo lang('text_backup') ?></a></li>
                </ul>
            </li>
            <li><a><i class="icon ion-gear-a"></i> <span class="title"><?php echo lang('text_settings') ?></span> <i class="icon ion-ios-arrow-left menu-carret"></i></a>
                <ul class="nav child-menu">
                    <li><a href="<?php echo site_url('admin/setting/general') ?>"><?php echo lang('text_general') ?></a></li>
                </ul>
            </li>
            <li><a><i class="icon ion-earth"></i> <span class="title"><?php echo lang('text_localisation') ?></span> <i class="icon ion-ios-arrow-left menu-carret"></i></a>
                <ul class="nav child-menu">
                    <li><a href="<?php echo site_url('admin/localisation/language') ?>"><?php echo lang('text_language') ?></a></li>
                </ul>
            </li>
            <?php if ($this->session->userdata['user_privilege'] == (int) 1) : ?>
            <li><a><i class="icon ion-person-stalker"></i> <span class="title"><?php echo lang('text_users') ?></span> <i class="icon ion-ios-arrow-left menu-carret"></i></a>
                <ul class="nav child-menu">
                    <li><a href="<?php echo site_url('admin/user') ?>"><?php echo lang('text_all_users') ?></a></li>
                    <li><a href="<?php echo site_url('admin/user_permission') ?>"><?php echo lang('text_user_permission') ?></a></li>
                </ul>
            </li>
            <?php else : ?>
            <li><a><i class="icon ion-person"></i> <span class="title"><?php echo lang('text_users') ?></span> <i class="icon ion-ios-arrow-left menu-carret"></i></a>
                <ul class="nav child-menu">
                    <li><a href="<?php echo site_url('admin/user/edit/' . $this->session->userdata['user_id']); ?>"><?php echo lang('text_profile') ?></a></li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</aside>