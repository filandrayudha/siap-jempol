# App Framework

## Instalasi

1. Buat file `application/config/constants.php`

```
<?php defined('BASEPATH') OR exit('No direct script access allowed');

defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb');
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b');
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');
defined('EXIT_SUCCESS')         OR define('EXIT_SUCCESS', 0);
defined('EXIT_ERROR')           OR define('EXIT_ERROR', 1);
defined('EXIT_CONFIG')          OR define('EXIT_CONFIG', 3);
defined('EXIT_UNKNOWN_FILE')    OR define('EXIT_UNKNOWN_FILE', 4);
defined('EXIT_UNKNOWN_CLASS')   OR define('EXIT_UNKNOWN_CLASS', 5);
defined('EXIT_UNKNOWN_METHOD')  OR define('EXIT_UNKNOWN_METHOD', 6);
defined('EXIT_USER_INPUT')      OR define('EXIT_USER_INPUT', 7);
defined('EXIT_DATABASE')        OR define('EXIT_DATABASE', 8);
defined('EXIT__AUTO_MIN')       OR define('EXIT__AUTO_MIN', 9);
defined('EXIT__AUTO_MAX')       OR define('EXIT__AUTO_MAX', 125);
defined('BASE_URL')             OR define('BASE_URL', 'http://localhost/app-framework/'); // URL Aplikasi
defined('ADMIN_ASSETS_URL')     OR define('ADMIN_ASSETS_URL', BASE_URL . 'views/backend/assets/');
defined('FRONTEND_URL')         OR define('FRONTEND_URL', BASE_URL . 'views/frontend/');
defined('THUMBNAIL_IMAGE_DEFAULT')      OR define('THUMBNAIL_IMAGE', 'uploads/images/default/default-thumbnail-image.png');
defined('THUMBNAIL_FILE_DEFAULT')       OR define('THUMBNAIL_FILE', 'uploads/images/default/default-thumbnail-file.png');
```

2. Buat file `application/config/database.php`

```
<?php defined('BASEPATH') OR exit('No direct script access allowed');

$active_group  = 'default';
$query_builder = TRUE;

$db['default'] = array(
    'dsn'          => '',
    'hostname'     => 'localhost',
    'username'     => 'johndoe',
    'password'     => 'toor',
    'database'     => 'app_framework',
    'dbdriver'     => 'mysqli',
    'dbprefix'     => '',
    'pconnect'     => FALSE,
    'db_debug'     => (ENVIRONMENT !== 'production'),
    'cache_on'     => FALSE,
    'cachedir'     => '',
    'char_set'     => 'utf8',
    'dbcollat'     => 'utf8_general_ci',
    'swap_pre'     => '',
    'encrypt'      => FALSE,
    'compress'     => FALSE,
    'stricton'     => FALSE,
    'failover'     => array(),
    'save_queries' => TRUE
);
```

>Sesuaikan dengan database yang digunakan

3. Buat file `.htaccess`

```
<IfModule mod_rewrite.c>

    Options +FollowSymLinks
    RewriteEngine on

    # Send request via index.php
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)$ index.php/$1 [L]

</IfModule>
```

untuk `https`

```
<IfModule mod_rewrite.c>

    Options +FollowSymLinks
    RewriteEngine on
    RewriteCond %{HTTPS} off
    RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

    # Send request via index.php
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule ^(.*)$ index.php/$1 [L]

</IfModule>

RewriteRule .*\.()$ - [F,NC]

RewriteRule .*\.()$ - [F,NC]
```

4. Import database `app_framework.sql`.

Itu saja!

Selamat bekerja.. :)

