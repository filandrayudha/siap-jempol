<?php
class Home extends Frontend_controller {

    public function __construct(){
        parent::__construct();
    }

    public function index() {

        $this->data['subview'] = 'frontend/' . $this->data['active_theme'] . '/' . 'home';
    	$this->load->view('frontend/' . $this->data['active_theme'] . '/_layout_main', $this->data);
    }
}