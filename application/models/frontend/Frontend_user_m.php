<?php
class Frontend_user_M extends CORE_Model
{
	protected $_table_name  = 'core_frontend_users';
	protected $_primary_key = 'md5(frontend_user_id)';
	protected $_order_by    = 'frontend_user_id';
	public $login_rules = array(
		'username' => array(
			'field' => 'username',
			'label' => 'Username',
			'rules' => 'trim|required'
		),
		'password' => array(
			'field' => 'password',
			'label' => 'Password',
			'rules' => 'trim|required'
		)
	);


	function __construct ()
	{
		parent::__construct();
	}

	// Get new user
	public function get_new_user()
	{
		$result = array(
			'user_id'          => '',
			'user_group_id'    => array(),
			'username'         => set_value('username'),
			'email'            => set_value('email'),
			'firstname'        => set_value('firstname'),
			'lastname'         => set_value('lastname'),
			'password'         => set_value('password'),
			'password_confirm' => set_value('password_confirm'),
			'status'           => set_value('status'),
			'image'            => set_value('image'),
			'image_path'       => base_url(THUMBNAIL_IMAGE),
			'date_added'       => set_value('date_added'),
			'loket_name'       => set_value('loket_name')
	    );

		$result['description'] = '';
		$result['social']      = array();

		return $result;
	}

	// Login
	public function login()
	{
		$user = $this->get_by(array(
			'username' => $this->input->post('username'),
			'password' => $this->hash($this->input->post('password'))
		), TRUE);

		if (count($user)) {
            if ($this->input->post('remember')) {
                $key = random_string('alnum', 64);
                set_cookie($this->config->item('sess_cookie_name'), $key, 3600*24*30); // set expired 30 hari kedepan

                // simpan key di database
                $update_key = array(
                    'cookie' => $key
                );
                $this->save($update_key, $user['user_id']);
			}

			$this->register_session($user);

			return TRUE;
		}

		return FALSE;
	}

	// Register session
	public function register_session($user)
	{
		if (count($user)) {
			$data_login = array(
				'user_id'        => md5($user['user_id']),
				'username'       => $user['username'],
				'firstname'      => $user['firstname'],
				'lastname'       => $user['lastname'],
				'email'          => $user['email'],
				'user_group_id'  => $user['user_group_id'],
				'user_privilege' => (int) $user['user_privilege'],
				'language_code'  => $user['language_code'],
				'image'          => $user['image'],
				'loggedin'       => TRUE
			);

			$this->session->set_userdata($data_login);
		}
	}

	public function logout()
	{
		delete_cookie($this->config->item('sess_cookie_name'));
		$this->session->sess_destroy();
	}

	// Check if loggedin
	public function frontend_loggedin()
	{
		return (bool) $this->session->userdata('frontend_loggedin');
	}

	// Hashing password
	public function hash($string)
	{
		return hash('sha512', $string . config_item('encryption_key'));
	}
}