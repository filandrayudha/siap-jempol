<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Class Frontend_Controller
* @author   Masriadi
*/
class Frontend_controller extends CORE_controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('frontend/frontend_user_m');

        // Check login user
        $exception_uris = array(
			'user/login',
			'user/logout',
			'user/register'
		);

		if (in_array(uri_string(), $exception_uris) == FALSE) {
			if ($this->frontend_user_m->frontend_loggedin() == FALSE) {
				// $redirect = current_url();
				redirect('/user/login');
			}
		}

        // Set Frontend Theme
        $this->load->model('setting/setting_m');
        if ($this->setting_m->get_setting('active_theme')) {
        	$this->data['active_theme'] = $this->setting_m->get_setting('active_theme');
		} else {
			$this->data['active_theme'] = 'default';
		}
    }
}